const SpritePlugin = require("svg-sprite-loader/plugin");

module.exports = {
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: [
          {
            loader: "svg-sprite-loader",
            // options: {
            //   runtimeCompat: true,
            //   // symbolId: filePath => filePath
            // }
          },
          "svgo-loader",
        ],
      },
    ],
  },
  plugins: [
    new SpritePlugin({
      // plainSprite: true,
      // spriteAttrs: {
      //   id: 'sprite'
      // }
    }),
  ],
};
