import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultSimpleModalOptionConfig, defaultSimpleModalOptions, SimpleModalModule} from 'ngx-simple-modal';
import { OrderModalComponent } from './order-modal/order-modal.component';
import { NewsModalComponent } from './news-modal/news-modal.component';
import { SharedModule } from "../../shared/shared.module";

@NgModule({
    declarations: [
        OrderModalComponent,
        NewsModalComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        SimpleModalModule.forRoot({container: document.body}),
    ],
    exports: [
        SimpleModalModule,
        OrderModalComponent,
        NewsModalComponent
    ],
    providers: [
        {
            provide: DefaultSimpleModalOptionConfig,
            useValue: { ...defaultSimpleModalOptions, ...{ closeOnEscape: true, closeOnClickOutside: true, wrapperClass: 'in show' } }
        },
    ]
})
export class ModalsModule { }
