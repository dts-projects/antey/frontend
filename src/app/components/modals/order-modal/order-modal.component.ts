import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SimpleModalComponent, SimpleModalService} from 'ngx-simple-modal';
import {Router} from '@angular/router';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import formFieldMeta from "../../../core/form/formFieldMeta";
import FormControlName from 'src/app/core/form/formControlName';
import fieldError from "../../../core/form/fieldError";

const animationDuration = 200;

// eslint-disable-next-line @typescript-eslint/naming-convention
interface Product {
 value: string;
 name: string
}

@Component({
  selector: 'app-order-modal',
  templateUrl: './order-modal.component.html',
  styleUrls: ['./order-modal.component.scss'],
  animations: [
    trigger('field', [
      transition(':enter', [
        style({opacity: 0}),
        animate(800)
      ])
    ]),
    trigger('stepOne', [
      state('one', style({ position: 'relative',  transform: 'translateX(0)', opacity: 1 })),
      state('two', style({position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, transform: 'translateX(-100%)', opacity: 0})),
      transition('one => two', animate(animationDuration, keyframes([
        style({ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, offset: 0 }),
        style({ transform: 'translateX(-100%)', offset: 1})
      ]))),
      transition('two => one', animate(animationDuration, keyframes([
        style({ opacity: 1, offset: 0 }),
        style({ transform: 'translateX(0)', offset: 1})
      ])))
    ]),
    trigger('stepTwo', [
      state('one', style({ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, transform: 'translateX(100%)', opacity: 0 })),
      state('two', style({position: 'relative', transform: 'translateX(0)', opacity: 1 })),
      transition('one => two', animate(animationDuration, keyframes([
        style({ position: 'relative', opacity: 1, offset: 0 }),
        style({ transform: 'translateX(0)', offset: 1})
      ]))),
      transition('two => one', animate(animationDuration, keyframes([
        style({ transform: 'translateX(100%)', offset: 1})
      ]))),
    ])
  ]
})
export class OrderModalComponent extends SimpleModalComponent<null, null> implements OnInit, OnDestroy {
  public type: string = '';
  public title: string  = '';
  public subtitle: string  = '';
  // @ts-ignore
  public products: any[] = [];
  public price: number  = 0;

  public entity: boolean = false;
  public FormFieldMeta = formFieldMeta;
  public FormControlName = FormControlName;
  public FormFieldError = fieldError;

  public form: FormGroup;
  public list = [];

  public totalSum = 0;

  stepTwo = false;

  FormSteps = {
    one: 'one',
    two: 'two',
    three: 'three'
  };

  SubmitState = {
    SENDING: 'sending',
    SUCCESS: 'success',
    FAIL: 'fail'
  };

  formSubmitState = '';
  currentStep: string = 'one';

  constructor() {
    super();

    this.form = new FormGroup({
      [FormControlName.Count]: new FormControl(1, [Validators.required]),
      [FormControlName.Delivery]: new FormControl(false, [Validators.required]),
      personal: new FormGroup({
        [FormControlName.FirstName]: new FormControl('', [Validators.required]),
        [FormControlName.Email]: new FormControl('', [Validators.required, Validators.email]),
        [FormControlName.Tel]: new FormControl('', [Validators.required]),
        [FormControlName.Message]: new FormControl('', [Validators.required])
      })
    });

    const deliveryGroup = new FormGroup({
      [FormControlName.Date]: new FormControl('', [Validators.required]),
      [FormControlName.Address]: new FormControl('', [Validators.required])
    });

    // @ts-ignore
    this.form.get(FormControlName.Delivery).valueChanges
      .subscribe((isVisible) => {

        if (isVisible) {
          this.form.addControl('delivery-group', deliveryGroup);
        } else {
          this.form.removeControl('delivery-group');
        }
      });

    if ((this.products && this.products.length)) {
      this.form.addControl(FormControlName.GoodsType, new FormControl(this.products[0].value, [Validators.required]));
    }
  }

  ngOnInit(): void {
    this.currentStep = this.type === 'product' ? this.FormSteps.one : this.FormSteps.two;
  }

  goToStep(step: string) {
    this.currentStep = step;
  }

  selectUserType(id: any) {
    this.entity = id === 'entity';
    console.log('id', id);

    if (id === 'entity') {
      // (this.form.get('personal') as FormGroup).removeControl('userName');
      (this.form.get('personal') as FormGroup).addControl('companyName', new FormControl('', [Validators.required]));
      // (this.form.get('personal') as FormGroup).addControl('companyPerson', new FormControl('', [Validators.required]));
    } else {
      // (this.form.get('personal') as FormGroup).addControl('userName', new FormControl('', [Validators.required]));
      (this.form.get('personal') as FormGroup).removeControl('companyName');
      // (this.form.get('personal') as FormGroup).removeControl('companyPerson');
    }
  }

  getObjectKey(object: any) {
    return (object instanceof Object) && Object.keys(object);
  }

  onSubmit() {
    if (!this.form.valid) { return; }


    // const variant = this.serviceList.filter((item) => {
    //   return this.form.value.variant === item.value;
    // })[0];

    // const formData = {...this.form.value, id: this.data.id, serviceTitle: this.data.name, variant: variant.name, date: new Date()};
    // console.log(formData);

    this.formSubmitState = this.SubmitState.SENDING;

    // this.af.collection('services-email').add(formData)
    //   .then((result) => {
    //     if (result) {
    //       this.formSubmitState = this.SubmitState.SUCCESS;
    //       this.form.reset();
    //     }
    //   })
    //   .catch((error) => {
    //     this.formSubmitState = this.SubmitState.FAIL;
    //   });
  }

  ngOnDestroy(): void {
    this.form.removeControl(FormControlName.GoodsType);
  }
}


