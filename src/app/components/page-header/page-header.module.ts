import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderComponent } from './page-header.component';
import {SharedModule} from '../../shared/shared.module';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ModalsModule} from '../modals/modals.module';

@NgModule({
  declarations: [
    PageHeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    ModalsModule,
    BrowserAnimationsModule
  ],
  exports: [
    PageHeaderComponent
  ]
})
export class PageHeaderModule { }
