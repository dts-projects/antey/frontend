import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { AngularYandexMapsModule, YaConfig } from 'angular8-yandex-maps';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent extends BaseComponent implements OnInit {
  // @Input() points: Array<any>;
  // @Input() latitude: string;
  // @Input() longitude: string;
  // @Input() zoom: string;
  // @Input() disableDefaultUI: boolean;
  // @Input() gestureHandling: boolean;
  // @Input() scrollwheel: boolean;
  // @Input() styles: any;
  // @Output() pointClick: EventEmitter<any> = new EventEmitter<any>();

  title = '';

  public imagesPath = '/assets/images/components/map/images/';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('map');
  }

  mapClick($event: MouseEvent) {}

  // pointChange($event: AgmMarker) {
  //   this.pointClick.emit($event.title);
  // }
}
