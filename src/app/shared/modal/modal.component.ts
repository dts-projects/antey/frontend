import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent extends BaseComponent implements OnInit {
  @Input() title: string = '';
  @Input() subtitle: string = '';
  @Input() meta: string = '';


  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('modal');
  }

  onClose() {
    this.close.emit();
  }
}
