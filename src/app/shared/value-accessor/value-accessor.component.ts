import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../base/base.component';
import {ControlValueAccessor} from '@angular/forms';

@Component({
  selector: 'app-value-accessor',
  templateUrl: './value-accessor.component.html',
  styleUrls: ['./value-accessor.component.scss']
})

export class ValueAccessorComponent extends BaseComponent implements ControlValueAccessor {
  public value: any;
  public stateDisabled = false;

  constructor() {
    super();
  }

  onChange: (_: any) => void = (_: any) => {};
  onTouched: () => void = () => {};

  changeValue(value: any) {
    this.value = value;
    this.onChange(value);
  }

  writeValue(value: boolean): void {
    this.value = value;
    // this.change.emit(value);
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState?(disabled: boolean): void {
    this.stateDisabled = disabled;
  }
}
