import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent {
  @Input() mods = '';
  @Input() invalid: boolean | undefined = false;
  @Input() disabled: boolean | undefined = false;

  public cssClass = '';

  constructor() { }

  setClass(cls: string, mods:string = this.mods) {
    return this.setMods(cls, mods);
  }

  private setMods(cls: string, mods: string) {
    let cssClass = cls;
    let allMods = '';

    if (mods !== 'undefined' && mods ) {
      const modsList = mods.split(',');
      for (const item of modsList) {
        allMods = allMods + ` ${cls}--` + item.trim();
      }
    }

    cssClass += allMods;

    return cssClass;
  }
}
