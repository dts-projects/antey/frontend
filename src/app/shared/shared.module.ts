import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconComponent } from './icon/icon.component';
import { BaseComponent } from './base/base.component';
import { BtnComponent } from './btn/btn.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { InputComponent } from './input/input.component';
import { LinkComponent } from './link/link.component';
import { LoaderComponent } from './loader/loader.component';
import { ModalComponent } from './modal/modal.component';
import { SelectComponent } from './select/select.component';
import { SwitcherComponent } from './switcher/switcher/switcher.component';
import { ValueAccessorComponent } from './value-accessor/value-accessor.component';
import { SwiperModule } from "swiper/angular";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NewsDetailsComponent } from "./news-details/news-details.component";
import { ImgComponent } from './img/img.component';

// mask
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { AngularMyDatePickerModule } from "angular-mydatepicker";
import { RouterModule } from "@angular/router";
// import { SupportFormComponent } from './support-form/support-form.component';
import { MapComponent } from "./map/map.component";
import { AngularYandexMapsModule, YaConfig } from "angular8-yandex-maps";
import { SupportFormComponent } from "./support-form/support-form.component";
import { CheckboxComponent } from "./checkbox/checkbox.component";
// import { BaseFormComponent } from "./base-form/base-form.component";
export let options: Partial<IConfig> = {
  validation: true,
  dropSpecialCharacters: false,
  // showMaskTyped: false,
  // clearIfNotMatch: true
};

const mapConfig: YaConfig = {
  apikey: 'API_KEY',
  lang: 'en_US',
};

@NgModule({
  declarations: [
    IconComponent,
    BaseComponent,
    BtnComponent,
    DatepickerComponent,
    InputComponent,
    LinkComponent,
    LoaderComponent,
    ModalComponent,
    SelectComponent,
    SwitcherComponent,
    NewsDetailsComponent,
    ImgComponent,
    SupportFormComponent,
    MapComponent,
    CheckboxComponent,
    // BaseFormComponent,
    ValueAccessorComponent
  ],
  imports: [
    CommonModule,
    SwiperModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AngularMyDatePickerModule,
    AngularYandexMapsModule.forRoot(mapConfig),
    NgxMaskModule.forRoot(options),
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    RouterModule,
    IconComponent,
    BaseComponent,
    BtnComponent,
    DatepickerComponent,
    InputComponent,
    LinkComponent,
    LoaderComponent,
    ModalComponent,
    SelectComponent,
    SwitcherComponent,
    NewsDetailsComponent,
    ImgComponent,
    AngularYandexMapsModule,
    SupportFormComponent,
    MapComponent,
    CheckboxComponent,
    // BaseFormComponent,
    ValueAccessorComponent,
  ]
})
export class SharedModule {}
