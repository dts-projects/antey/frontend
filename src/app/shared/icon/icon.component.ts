import { Component, Input, OnInit } from '@angular/core';

// svg
// import './svg/clock.svg';
// import './svg/mail.svg';
// import './svg/phone.svg';
// import './svg/place.svg';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})

export class IconComponent implements OnInit {
  @Input() width: string = '';
  @Input() height: string = '';
  @Input() name: string = '';
  @Input() mods: string = '';

  public cssClass = '';

  constructor() {}

  ngOnInit(): void {
    this.cssClass = this.setMods('icon', this.mods);
  }

  private setMods(cls: string, mods: string) {
    let cssClass = cls;
    let allMods = '';

    if (mods !== 'undefined' && mods) {
      const modsList = mods.split(',');
      for (const item of modsList) {
        allMods = allMods + ` ${cls}--` + item.trim();
      }
    }

    cssClass += allMods;

    return cssClass;
  }
}
