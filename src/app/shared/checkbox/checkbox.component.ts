import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {NG_VALUE_ACCESSOR} from '@angular/forms';
import {ValueAccessorComponent} from '../value-accessor/value-accessor.component';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CheckboxComponent),
    multi: true
  }]
})

export class CheckboxComponent extends ValueAccessorComponent implements OnInit {
  @Input() id = '';
  @Input() name = '';
  @Input() isClear = false;

  // @ts-ignore
  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  @Output() clear: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    super();
  }

  override changeValue(value: any) {
    super.changeValue(value);
    this.change.emit(value);
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('checkbox');
  }

  onClear(event: Event) {
    this.clear.emit(event);
  }
}
