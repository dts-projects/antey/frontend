import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from "@angular/forms";
import {Router} from '@angular/router';
import FormControlName from "../../core/form/formControlName";
import { Pattern } from "../../core/pattern/pattern";
import formFieldMeta from "../../core/form/formFieldMeta";
import { SimpleModalService } from "ngx-simple-modal";
import { KeyValue } from "@angular/common";

@Component({
  selector: 'app-support-form',
  templateUrl: './support-form.component.html',
  styleUrls: ['./support-form.component.scss']
})
export class SupportFormComponent implements OnInit {
  FormFieldMeta = formFieldMeta;
  FormControlName = FormControlName;

  form: FormGroup;
  isLoading = false;

  constructor(public simpleModal: SimpleModalService,
              public router: Router) {
    this.form = new FormGroup({
      [FormControlName.FirstName]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Text),
          Validators.minLength(2)]),
      [FormControlName.Tel]: new FormControl('',
        [Validators.required, Validators.pattern(Pattern.Phone)]),
      [FormControlName.Email]: new FormControl('',
        [Validators.required, Validators.email]),
      [FormControlName.Question]: new FormControl('',
        [Validators.required,  Validators.pattern(Pattern.TextWithNumbersAndSymbols),
          Validators.minLength(2)]),
      [FormControlName.Agree]: new FormControl(false, [Validators.required]),
      [FormControlName.Captcha]: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
    console.log('dddd')

  }

  getGroupControls(group: FormGroup) {
    // console.log((group as FormGroup).controls)
    return group.controls;
  }

  originalOrder = (a: KeyValue<string, AbstractControl>, b: KeyValue<string, AbstractControl>): number => {
    return 0;
  }

  onSubmit() {
    const data = this.form?.value;
  }
}
