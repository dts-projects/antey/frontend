import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

// eslint-disable-next-line @typescript-eslint/naming-convention
interface Breakpoints {
  sm: boolean;
  md: boolean;
  lg: boolean;
  xl: boolean;
}

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})

export class ImgComponent extends BaseComponent implements OnInit {
  @Input() width = 0;
  @Input() height = 0;
  @Input() maxWidth = '';
  @Input() name = '';
  @Input() ext = 'jpg';
  @Input() alt = '';
  @Input() breakpoints?: Breakpoints;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('img');
  }
}
