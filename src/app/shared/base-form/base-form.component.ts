// import { Component, OnInit } from '@angular/core';
// import {SimpleModalService} from 'ngx-simple-modal';
// import {Router} from '@angular/router';
// import {FormGroup} from '@angular/forms';
// import FormControlName from "../../core/form/formControlName";
//
// // import {CommonService} from '../../../core/services/common/common.service';
//
// @Component({
//   selector: 'app-base-form',
//   templateUrl: './base-form.component.html',
//   styleUrls: ['./base-form.component.scss']
// })
// export class BaseFormComponent {
//   public form: FormGroup | null = null;
//   public isLoading = false;
//
//   constructor(
//     public simpleModal: SimpleModalService,
//     public router: Router,
//     public commonService: any
//     ) { }
//
//   onSubmit(data: any) {
//     this.form?.markAllAsTouched();
//
//     if (this.form?.invalid) {
//       return;
//     }
//
//     this.isLoading = true;
//
//     // this.commonService.sendMail({
//     //   'api-key': environment.api_key,
//     //    ...data
//     // })
//     //   .pipe(take(1))
//     //   .subscribe(() => {
//     //       this.isLoading = false;
//     //       this.alert('Ваше сообщение отправлено!<br /> Мы свяжемся с Вами в ближайшее время!')
//     //         .pipe(take(1))
//     //         .subscribe(() => {
//     //           this.form?.reset();
//     //         });
//     //     },
//     //     (error: any) => {
//     //       this.isLoading = false;
//     //       this.alert('Ой, что-то пошло не так!<br /> Сообщение не было отправлено!', 'Понятно!')
//     //         .pipe(take(1))
//     //         .subscribe(() => {});
//     //     });
//   }
//
//   captchaResolved(value: string) {
//     if (!value) {
//       this.form?.get(FormControlName.Agree)?.setValue(false);
//     }
//   }
//
//   executeCaptcha(captcha: any) {
//     captcha.execute();
//     this.form?.markAllAsTouched();
//   }
//
//   // confirm(e: Event) {
//   //   e.preventDefault();
//   //
//   //   this.simpleModal.addModal(ConfirmModalComponent, {
//   //     message: 'Перейти на страницу "Политика конфиденциальности"?'
//   //   }).subscribe((isConfirmed) => {
//   //     if (isConfirmed) {
//   //       this.router.navigate([]).then((result) => {
//   //         window.open('/info/privacy-policy', '_blank');
//   //       });
//   //     }
//   //   });
//   // }
//   //
//   // alert(message: string, btnText = null) {
//   //   return this.simpleModal.addModal(AlertModalComponent, {
//   //     message,
//   //     btnText
//   //   });
//   // }
// }
