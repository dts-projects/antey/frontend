import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-switcher',
  templateUrl: './switcher.component.html',
  styleUrls: ['./switcher.component.scss']
})
export class SwitcherComponent{
  @Input() name = '';
  @Input() items: Array<any> = [];
  @Output() switch: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  switchType(id: string) {
    this.switch.emit(id);
  }
}
