import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-btn',
  templateUrl: './btn.component.html',
  styleUrls: ['./btn.component.scss']
})

export class BtnComponent extends BaseComponent implements OnInit {
  @Input() id = ''
  @Input() type = '';
  @Input() hidden: boolean = false;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('btn');
  }
}
