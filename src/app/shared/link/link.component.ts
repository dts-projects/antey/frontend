import {Component, Input, OnInit} from '@angular/core';
import {BaseComponent} from '../base/base.component';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent extends BaseComponent implements OnInit {
  @Input() id = '';
  @Input() href = '';

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.cssClass = this.setClass('link');
  }
}
