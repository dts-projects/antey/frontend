import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndexPageModule } from "./pages/index-page/index-page.module";
import { NotFoundPageModule } from "./pages/not-found-page/not-found-page.module";
import { PageHeaderModule } from "./components/page-header/page-header.module";
import { SharedModule } from './shared/shared.module';
import { PageFooterModule } from "./components/page-footer/page-footer.module";
import { AngularYandexMapsModule } from "angular8-yandex-maps";

@NgModule({
  declarations: [AppComponent],
  imports: [
    PageHeaderModule,
    PageFooterModule,
    IndexPageModule,
    NotFoundPageModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
