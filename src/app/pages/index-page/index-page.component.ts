import { Component, OnInit } from '@angular/core';
// import { HelloService } from 'src/app/core/services/hello.service';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('hello');
    // this.helloService.getHello()
    //   .subscribe((text: string) => {
    //     console.log('text', text);
    //   });
  }

}
