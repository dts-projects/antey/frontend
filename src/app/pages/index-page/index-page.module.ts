import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IndexPageComponent} from './index-page.component';
import {SharedModule} from '../../shared/shared.module';
import { MainSliderComponent } from './components/main-slider/main-slider.component';
import { AboutShortComponent } from './components/about-short/about-short.component';
import { ServicesComponent } from './components/services/services.component';
import { ProductsComponent } from './components/products/products.component';
import { PromoComponent } from './components/promo/promo.component';
import { NewsComponent } from './components/news/news.component';
import { NewsSliderComponent } from './components/news-slider/news-slider.component';
import { ArticleComponent } from './components/article/article.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { SupportComponent } from './components/support/support.component';
import { GoodsComponent } from './components/goods/goods.component';
import { ProductsSliderComponent } from './components/products-slider/products-slider.component';
import { ProductComponent } from './components/product/product.component';
import { ModalsModule } from "../../components/modals/modals.module";

@NgModule({
    declarations: [
        IndexPageComponent,
        MainSliderComponent,
        AboutShortComponent,
        ServicesComponent,
        ProductsComponent,
        PromoComponent,
        NewsComponent,
        NewsSliderComponent,
        ArticleComponent,
        ContactsComponent,
        SupportComponent,
        GoodsComponent,
        ProductsSliderComponent,
        ProductComponent,
    ],
    imports: [
        CommonModule,
        ModalsModule,
        SharedModule
    ]
})
export class IndexPageModule { }
