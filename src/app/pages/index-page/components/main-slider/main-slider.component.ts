import { AfterViewChecked, Component, Input, OnInit, ViewChild } from "@angular/core";
import {SwiperComponent} from "swiper/angular";
import {OrderModalComponent} from "../../../../components/modals/order-modal/order-modal.component";
import {SimpleModalService} from "ngx-simple-modal";

@Component({
  selector: 'app-main-slider',
  templateUrl: './main-slider.component.html',
  styleUrls: ['./main-slider.component.scss']
})
export class MainSliderComponent implements OnInit {
  @ViewChild('swiper', { static: false }) swiper?: SwiperComponent;

  @Input() id: string = '';
  @Input() config: any;


  public data = [
    {
      type: 'sale',
      title: 'Оптово-розничная реализациия нефтепродуктов',
      text: 'Поставка качественных нефтепродуктов на гибких условиях – бензин, дизельное топливо'
    },
    {
      type: 'exchange',
      title: 'Транспортные<br /> услуги по перевозки опасных грузов ',
      text: 'Перевозка грузов собственным автотранспортом, отгрузка жд цистернами, прием, слив вагоно-цистерн'
    },
    {
      type: 'transit',
      title: 'Закупка нефтепродуктов на <br />товарно-сырьевой бирже',
      text: 'Аккредитованный участник торгов, гибкие условия приобретения, минимальная комиссия'
    }
  ];

  constructor(private modalService: SimpleModalService) {}

  ngOnInit(): void {
console.log('asdasdas')
    this.config = {
      slidesPerView: 1,
      spaceBetween: 0,
      centeredSlides: true,
      loop: true,
      breakpointsInverse: true,
      breakpoints: {
        // [MediaQuery.S]: {
        //   slidesPerView: 2,
        //   // spaceBetween: 20
        // },
        // [MediaQuery.SM]: {
        //   slidesPerView: 2,
        //   // spaceBetween: 20
        // },
        // [MediaQuery.MD]: {
        //   slidesPerView: 3,
        //   // spaceBetween: 20
        // }
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true}
    };
  }

  slideNext() {
    this.swiper?.swiperRef.slideNext(100);
  }

  slidePrev() {
    this.swiper?.swiperRef.slidePrev(100);
  }

  showModal(data: any) {
    this.modalService.addModal(OrderModalComponent, {
      type: 'service',
      title: data.title,
      subtitle: data.subtitle
    });
  }
}
