import {Component, Input, OnInit} from '@angular/core';
import {SimpleModalService} from 'ngx-simple-modal';
import {NewsModalComponent} from '../../../../components/modals/news-modal/news-modal.component';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent {
  @Input() data: any;

  public imagesPath = '/assets/images/pages/index-page/components/article/images';

  constructor(private modalService: SimpleModalService) { }

  showModal(e: Event, data: any) {
    e.preventDefault();

    this.modalService.addModal(NewsModalComponent, {
      id: '666',
      title: data.title,
      text: data.text,
      date: data.date
    });
  }

  trimText(text: string) {
    return text.slice(0, 200) + '...';
  }
}
