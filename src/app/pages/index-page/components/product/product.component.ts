import {Component, Input, OnInit} from '@angular/core';
import {SimpleModalService} from 'ngx-simple-modal';
import {OrderModalComponent} from "../../../../components/modals/order-modal/order-modal.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent {
  @Input() data: any;

  constructor(private modalService: SimpleModalService) { }

  showModal(data: any) {
    // this.modalService.addModal(OrderModalComponent, {
    //   title,
    //   list: of(cities)
    // });

    this.modalService.addModal(OrderModalComponent, {
      type: 'product',
      title: data.title,
      subtitle: 'Безналичный расчет с НДС, без НДС',
      products: data.products,
      price: data.price
    });
  }
}
