import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {NotFoundPageComponent} from './not-found-page.component';



@NgModule({
  declarations: [
    NotFoundPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class NotFoundPageModule { }
